## Task_01
### Description of the task
Deploy new web-site in test environment. Back-end on Java, compiler Maven or Cradle, 
Nginx like a balancer. All instances must be in containers. Create CI for dev-branch. 
#### Check-points:
-  Back and front run as Docker containers, must connect to each other
-  Add Nginx before front, close all external connections, except to Nginx
-  Create docker-compose file, which run all instances
-  Multistaging in Dockerfile 
-  Create CI pipe
-  If we have commit in dev-branch, we must update apps
### Result of job
- 1 step: create images(mysql,backend,frontend,nginx)
- 2 step: run containers in dependency from instruction in docker-compose
- 3 step: as a result we can receive static files of site from front or can get api-request from back
- 4 step: if we have commit in branch Dev_Ops, we start CI/CD pipeline